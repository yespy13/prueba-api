var apiKey = '3b2b7855';
botonBuscar = document.getElementById("buscarPeli");
titulo = document.getElementById("titulo");
listaDatos = document.getElementById("listaDatos");

botonBuscar.addEventListener('click', cogerInfo);

async function cogerInfo() {
    reiniciar();
    var peli = document.getElementById("peliABuscar").value;
    console.log("http://www.omdbapi.com/?apikey=" + apiKey + "&t='" + peli + "'");
    var response = await fetch("http://www.omdbapi.com/?apikey=" + apiKey + "&t='" + peli + "'");
    var data = await response.json();
    console.log(data);
    titulo.innerHTML = data.Title;
    document.getElementById("poster").src = data.Poster;
    listaDatos.insertAdjacentHTML('beforeend', "<li>Director: " + data.Director + "</li>");
    listaDatos.insertAdjacentHTML('beforeend', "<li>Actores: " + data.Actors + "</li>");
    listaDatos.insertAdjacentHTML('beforeend', "<li>Tipo: " + data.Type + "</li>");
    listaDatos.insertAdjacentHTML('beforeend', "<li>Año: " + data.Year + "</li>");
    listaDatos.insertAdjacentHTML('beforeend', "<li>Estreno: " + data.Released + "</li>");
    listaDatos.insertAdjacentHTML('beforeend', "<li>Duración: " + data.Runtime + "</li>");
    listaDatos.insertAdjacentHTML('beforeend', "<li>Género: " + data.Genre + "</li>");
    listaDatos.insertAdjacentHTML('beforeend', "<li>Premios: " + data.Awards + "</li>");
    listaDatos.insertAdjacentHTML('beforeend', "<li>Valoración en IMDB: " + data.imdbRating + "</li>");
}

function reiniciar() {
    titulo.innerHTML = "OMDB";
    listaDatos.innerHTML = "";
}