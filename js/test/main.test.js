const { devuelveUno } = require('../main.js')

test ("Comprobamos que 1 es igual a 1", function() {
    expect(devuelveUno()).toBe(1);
});